package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;


public final class DefenciveSubsystemImpl implements DefenciveSubsystem{

    private String name;
    private final PositiveInteger powergridConsumption;
    private final PositiveInteger capacitorConsumption;
    private final PositiveInteger impactReductionPercent;
    private final PositiveInteger shieldRegeneration;
    private final PositiveInteger hullRegeneration;


    public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
                                                   PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
                                                   PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

        return new DefenciveSubsystemImpl(name, powergridConsumption, capacitorConsumption,
                impactReductionPercent, shieldRegeneration, hullRegeneration);
    }

    private DefenciveSubsystemImpl(String name, PositiveInteger powergridConsumption, PositiveInteger capacitorConsumption,
                                   PositiveInteger impactReductionPercent, PositiveInteger shieldRegeneration,
                                   PositiveInteger hullRegeneration) {
        this.setName(name);
        this.powergridConsumption = powergridConsumption;
        this.capacitorConsumption = capacitorConsumption;
        this.impactReductionPercent = impactReductionPercent;
        this.shieldRegeneration = shieldRegeneration;
        this.hullRegeneration = hullRegeneration;
    }

    public void setName(String name) {

        if (name == null || name.strip().equals("")) {
            throw new IllegalArgumentException("Name should be not null and not empty");
        }

        this.name = name;
    }

    @Override
    public PositiveInteger getPowerGridConsumption() {
        return this.powergridConsumption;
    }

    @Override
    public PositiveInteger getCapacitorConsumption() {
        return this.capacitorConsumption;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public AttackAction reduceDamage(AttackAction incomingDamage) {
        PositiveInteger damage;

            if (this.impactReductionPercent.value() < 95) {
                double reducedDamage = (double) incomingDamage.damage.value()
                        * ((double) this.impactReductionPercent.value() / 100);

                int impactDamage = (int) Math.ceil(incomingDamage.damage.value() - reducedDamage);


                damage = new PositiveInteger(impactDamage);


            }
            else{

                damage = PositiveInteger.of((int)Math.ceil(incomingDamage.damage.value() * 0.05));
            }

        return new AttackAction(damage, incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
    }

    @Override
    public RegenerateAction regenerate() {

        return new RegenerateAction(this.shieldRegeneration, this.hullRegeneration);
    }

    @Override
    public DefenciveSubsystemImpl getSubsystem() {
        return this;
    }

    public PositiveInteger getShieldRegeneration() {
        return shieldRegeneration;
    }

    public PositiveInteger getHullRegeneration() {
        return hullRegeneration;
    }
}
