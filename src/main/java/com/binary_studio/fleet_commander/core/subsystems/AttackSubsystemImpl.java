package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

import static com.google.common.primitives.Doubles.min;

public final class AttackSubsystemImpl implements AttackSubsystem {

    private String name;
    private PositiveInteger optimalSize;
    private PositiveInteger powergridRequirments;
    private PositiveInteger capacitorConsumption;
    private PositiveInteger optimalSpeed;
    private PositiveInteger baseDamage;

    private AttackSubsystemImpl(String name, PositiveInteger optimalSize, PositiveInteger powergridRequirments,
                                PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed,
                                PositiveInteger baseDamage) {
        this.setName(name);
        this.optimalSize = optimalSize;
        this.powergridRequirments = powergridRequirments;
        this.capacitorConsumption = capacitorConsumption;
        this.optimalSpeed = optimalSpeed;
        this.baseDamage = baseDamage;
    }

    public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
                                                PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
                                                PositiveInteger baseDamage) throws IllegalArgumentException {

        return new AttackSubsystemImpl(name, optimalSize, powergridRequirments,
                capacitorConsumption, optimalSpeed, baseDamage);
    }

    public void setOptimalSize(PositiveInteger optimalSize) {
        this.optimalSize = optimalSize;
    }

    public void setPowergridRequirments(PositiveInteger powergridRequirments) {
        this.powergridRequirments = powergridRequirments;
    }

    public void setCapacitorConsumption(PositiveInteger capacitorConsumption) {
        this.capacitorConsumption = capacitorConsumption;
    }

    public void setOptimalSpeed(PositiveInteger optimalSpeed) {
        this.optimalSpeed = optimalSpeed;
    }

    public void setBaseDamage(PositiveInteger baseDamage) {
        this.baseDamage = baseDamage;
    }

    public void setName(String name) {

        if (name == null || name.strip().equals("")) {
            throw new IllegalArgumentException("Name should be not null and not empty");
        }

        this.name = name;
    }

    @Override
    public PositiveInteger getPowerGridConsumption() {

        return this.powergridRequirments;
    }

    @Override
    public PositiveInteger getCapacitorConsumption() {

        return this.capacitorConsumption;
    }

    @Override
    public PositiveInteger attack(Attackable target) {

        double sizeReductionModifier = target.getSize().value() >= this.optimalSize.value() ? 1
                : (double) target.getSize().value() / (double) this.optimalSize.value();

        double speedReductionModifier = target.getCurrentSpeed().value() <= this.optimalSpeed.value() ? 1
                : (double) this.optimalSpeed.value() / (double) (2 * target.getCurrentSpeed().value());


        return new PositiveInteger((int) Math.ceil(this.baseDamage.value() * min(speedReductionModifier
                , sizeReductionModifier)));
    }

    @Override
    public String getName() {

        return this.name;
    }
}
