package com.binary_studio.fleet_commander.core.actions.attack;

import com.binary_studio.fleet_commander.core.common.NamedEntity;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;

import java.util.Objects;

public final class AttackAction {

	public final PositiveInteger damage;

	public final NamedEntity target;

	public final NamedEntity attacker;

	public final NamedEntity weapon;

	public AttackAction(PositiveInteger damage, NamedEntity attacker, NamedEntity target, NamedEntity weapon) {
		this.damage = damage;
		this.target = target;
		this.weapon = weapon;
		this.attacker = attacker;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		AttackAction action = (AttackAction) o;
		return Objects.equals(damage, action.damage) && Objects.equals(target, action.target) && Objects.equals(attacker, action.attacker) && Objects.equals(weapon, action.weapon);
	}

	@Override
	public int hashCode() {
		return Objects.hash(damage, target, attacker, weapon);
	}
}
