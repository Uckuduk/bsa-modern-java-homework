package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.AttackSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

    private final String name;
    private final PositiveInteger shieldHP;
    private PositiveInteger currentShieldHP;
    private final PositiveInteger hullHP;
    private PositiveInteger currentHullHP;
    private final PositiveInteger powergridOutput;
    private final PositiveInteger capacitorAmount;
    private PositiveInteger currentCapacitorAmount;
    private final PositiveInteger capacitorRechargeRate;
    private final PositiveInteger speed;
    private final PositiveInteger size;
    private final AttackSubsystem attackSubsystem;
    private final DefenciveSubsystem defenciveSubsystem;


    public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
                           PositiveInteger powergridOutput, PositiveInteger capacitorAmount,
                           PositiveInteger capacitorRechargeRate, PositiveInteger speed, PositiveInteger size,
                           AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
        this.name = name;
        this.shieldHP = shieldHP;
        this.currentShieldHP = shieldHP;
        this.hullHP = hullHP;
        this.currentHullHP = hullHP;
        this.powergridOutput = powergridOutput;
        this.capacitorAmount = capacitorAmount;
        this.currentCapacitorAmount = capacitorAmount;
        this.capacitorRechargeRate = capacitorRechargeRate;
        this.speed = speed;
        this.size = size;
        this.attackSubsystem = attackSubsystem;
        this.defenciveSubsystem = defenciveSubsystem;
    }

    @Override
    public void endTurn() {

    	if(this.currentCapacitorAmount.value() + this.capacitorRechargeRate.value() >= this.capacitorAmount.value()){
    		this.currentCapacitorAmount = this.capacitorAmount;
		}
    	else{
    		this.currentCapacitorAmount = new PositiveInteger(this.currentCapacitorAmount.value()
					+ this.capacitorRechargeRate.value());
		}

    }

    @Override
    public void startTurn() {

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public PositiveInteger getSize() {
        return size;
    }

    @Override
    public PositiveInteger getCurrentSpeed() {
        return speed;
    }

    @Override
    public Optional<AttackAction> attack(Attackable target) {
        Optional<AttackAction> attackAction;

        if(this.currentCapacitorAmount.value() < this.attackSubsystem.getCapacitorConsumption().value()){

            attackAction = Optional.empty();
        }
        else{
            int currentCapacity = this.currentCapacitorAmount.value() - attackSubsystem.getCapacitorConsumption().value();
            this.currentCapacitorAmount = new PositiveInteger(currentCapacity);
            AttackAction attack = new AttackAction(this.attackSubsystem.attack(target), this,
                    target, this.attackSubsystem);

            attackAction = Optional.of(attack);
        }

        return attackAction;
    }

    @Override
    public AttackResult applyAttack(AttackAction attack) {
        int shieldHP, hullHP;
        AttackResult result;
        AttackAction damage = defenciveSubsystem.reduceDamage(attack);

        if(damage.damage.value() < currentShieldHP.value() + currentHullHP.value()) {
            if (damage.damage.value() < this.currentShieldHP.value()) {
                shieldHP = this.currentShieldHP.value() - damage.damage.value();
                this.currentShieldHP = new PositiveInteger(shieldHP);
            } else {
                hullHP = this.currentHullHP.value() - (damage.damage.value() - this.currentShieldHP.value());
                this.currentShieldHP = new PositiveInteger(0);
                this.currentHullHP = new PositiveInteger(hullHP);
            }

            result = new AttackResult.DamageRecived(attack.weapon, damage.damage, attack.target);
        }else{

            result = new AttackResult.Destroyed();
        }


        return result;
    }

    @Override
    public Optional<RegenerateAction> regenerate() {
        Optional<RegenerateAction> regen;

        if(this.currentCapacitorAmount.value() < this.defenciveSubsystem.getCapacitorConsumption().value()){
            regen = Optional.empty();
        }
        else{
            int currentCapacity = this.currentCapacitorAmount.value() - defenciveSubsystem.getCapacitorConsumption().value();
            this.currentCapacitorAmount = new PositiveInteger(currentCapacity);
            regen = Optional.of(defenciveSubsystem.regenerate());

            if(this.defenciveSubsystem.getSubsystem().getHullRegeneration().value()
                    > this.hullHP.value() - this.currentHullHP.value()){
                regen.get().hullHPRegenerated
                        = new PositiveInteger(this.hullHP.value() - this.currentHullHP.value());
            }

            if(this.defenciveSubsystem.getSubsystem().getShieldRegeneration().value()
                    > this.shieldHP.value() - this.currentShieldHP.value()){
                regen.get().shieldHPRegenerated
                        = new PositiveInteger(this.shieldHP.value() - this.currentShieldHP.value());
            }

        }

        return regen;
    }
}
