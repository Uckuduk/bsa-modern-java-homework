package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

    private String name;
    private final PositiveInteger shieldHP;
    private final PositiveInteger hullHP;
    private final PositiveInteger powergridOutput;
    private final PositiveInteger capacitorAmount;
    private final PositiveInteger capacitorRechargeRate;
    private final PositiveInteger speed;
    private final PositiveInteger size;
    private AttackSubsystem attackSubsystem;
    private boolean attackSubsystemSet;
    private DefenciveSubsystem defenciveSubsystem;
    private boolean defenciveSubsystemSet;


    public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
                                       PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
                                       PositiveInteger speed, PositiveInteger size) {



        return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount,
                capacitorRechargeRate, speed, size);
    }

    private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
                      PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
                      PositiveInteger speed, PositiveInteger size) {
        this.setName(name);
        this.shieldHP = shieldHP;
        this.hullHP = hullHP;
        this.powergridOutput = powergridOutput;
        this.capacitorAmount = capacitorAmount;
        this.capacitorRechargeRate = capacitorRechargeRate;
        this.speed = speed;
        this.size = size;
        this.attackSubsystem = null;
        this.attackSubsystemSet = false;
        this.defenciveSubsystem = null;
        this.defenciveSubsystemSet = false;
    }

    public void setName(String name) {

        if (name == null || name.strip().equals("")) {
            throw new IllegalArgumentException("Name should be not null and not empty");
        }

        this.name = name;
    }

    @Override
    public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {

        if (subsystem != null) {
            int accessiblePowerGrid;
            if (this.defenciveSubsystem != null) {
                accessiblePowerGrid = this.powergridOutput.value()
                        - this.defenciveSubsystem.getPowerGridConsumption().value();
            } else {
                accessiblePowerGrid = this.powergridOutput.value();
            }

            this.attackSubsystemSet = true;

            if (accessiblePowerGrid < subsystem.getPowerGridConsumption().value()) {

                throw new InsufficientPowergridException(subsystem.getPowerGridConsumption().value() - accessiblePowerGrid);
            }
        }else{
            this.attackSubsystemSet = false;
        }

        this.attackSubsystem = subsystem;
    }

    @Override
    public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
        if (subsystem != null) {
            int accessiblePowerGrid;
            if (this.attackSubsystem != null) {
                accessiblePowerGrid = this.powergridOutput.value()
                        - this.attackSubsystem.getPowerGridConsumption().value();
            } else {
                accessiblePowerGrid = this.powergridOutput.value();
            }

            this.defenciveSubsystemSet = true;

            if (accessiblePowerGrid < subsystem.getPowerGridConsumption().value()) {

                throw new InsufficientPowergridException(subsystem.getPowerGridConsumption().value() - accessiblePowerGrid);
            }
        }else{
            this.defenciveSubsystemSet = false;
        }

        this.defenciveSubsystem = subsystem;
    }

    public CombatReadyShip undock() throws NotAllSubsystemsFitted {


        if (!this.defenciveSubsystemSet && !this.attackSubsystemSet) {
            throw NotAllSubsystemsFitted.bothMissing();

        } else if (!this.defenciveSubsystemSet) {
            throw NotAllSubsystemsFitted.defenciveMissing();

        } else if (!this.attackSubsystemSet) {
            throw NotAllSubsystemsFitted.attackMissing();
        }

        return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.powergridOutput,
                this.capacitorAmount, this.capacitorRechargeRate,
                this.speed, this.size, this.attackSubsystem, this.defenciveSubsystem);
    }

}
