package com.binary_studio.dependency_detector;

import java.util.List;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		boolean ready = true;

		for (String lib : libraries.libraries) {

			boolean firstPosition = false;
			boolean secondPosition = false;

			for (String[] dependence: libraries.dependencies) {
				if(lib.equals(dependence[0])){
					firstPosition = true;
				}

				if(lib.equals(dependence[1])){
					secondPosition = true;
				}
			}

			if(firstPosition && secondPosition){
				ready = false;
				break;
			}
		}

		return ready;
	}

}
