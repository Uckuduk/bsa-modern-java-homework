package com.binary_studio.academy_coin;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class AcademyCoin {

    private AcademyCoin() {
    }

    public static int maxProfit(Stream<Integer> prices) {

        int maxProfit = 0;

        if(prices != null) {

            List<Integer> priceList = prices.collect(Collectors.toList());

            if(priceList.size() != 0) {

                Iterator<Integer> iterator1 = priceList.iterator();


                for (Integer integer : priceList.subList(1, priceList.size())) {

                    int currentPrice = iterator1.next();
                    int nextPrice = integer;

                    if (currentPrice < nextPrice) {
                        maxProfit += (nextPrice - currentPrice);
                    }

                }
            }
        }

        return maxProfit;
    }

}
